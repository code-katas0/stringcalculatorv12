﻿using NUnit.Framework;
using StringCalculatorV12;
using System;

namespace StringCalculatorTest
{
    [TestFixture]
    public class StringCalculatorTest
    {
        private StringCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
            _calculator = new StringCalculator();
        }

        [Test]
        public void GivenEmptyString_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 0;

            //Act
            var actual = _calculator.Add("");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenOneNumber_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 1;

            //Act
            var actual = _calculator.Add("1");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenTwoNumber_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("1,2");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenUnlimitedNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("1,2,2,1");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenNewLineAsDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("1\n2,2,1");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenCustomDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 5;

            //Act
            var actual = _calculator.Add("//;\n2;2;1");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenNegativenumbers_WhenAdding_ThrowExpection()
        {
            //Arrange
            var expected = "Negatives not Allowed. -3 -4";

            //Act
            var expection =Assert.Throws<Exception>(()=> _calculator.Add("//;\n-3;-4"));

            //Assert
            Assert.AreEqual(expected, expection.Message);
        }

        [Test]
        public void GivenAnyLengthDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 5;

            //Act
            var actual = _calculator.Add("//***\n2***2***1");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenMultipleDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 5;

            //Act
            var actual = _calculator.Add("//[*][%]\n2*2%1");

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
