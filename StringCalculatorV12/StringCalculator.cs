﻿using System;
using System.Collections.Generic;

namespace StringCalculatorV12
{
    public class StringCalculator
    {
        private char comma = ',';
        private char newLine = '\n';
        private char slash = '/';
        private char leftSquare = '[';
        private char rightSquare = ']';
        private string doubleSlash = "//";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var delimiters = GetDelimiters(numbers);
            var total = GetTotal(numbers,delimiters);

            return total;
        }

        private List<string> GetDelimiters(string numbers)
        {
            var delimiters = new List<string>(new string[] { comma.ToString(),newLine.ToString()});

            if (numbers.Contains(doubleSlash))
            {
                numbers = string.Concat(numbers.Split(slash));
                var delimiter = numbers.Substring(0, numbers.IndexOf(newLine));

                delimiters.Clear();
                delimiters.Add(delimiter);
            }

            if (numbers.Contains(leftSquare.ToString()))
            {
                numbers = string.Concat(numbers.Split(slash));
                var delimiterArray = numbers.Substring(0, numbers.IndexOf(newLine)).Split(leftSquare,rightSquare);

                delimiters.Clear();

                foreach (var delimiter in delimiterArray)
                {
                    delimiters.Add(delimiter);
                }
            }

            return delimiters;
        }
        
        private int GetTotal(string numbers, List<string>delimiters)
        {
            var sum = 0;
            var negativeNumbers = string.Empty;
            if (numbers.Contains(doubleSlash))
            {
                numbers = string.Concat(numbers.Split(slash,newLine,leftSquare,rightSquare));
            }

            foreach (var number in numbers.Split(delimiters.ToArray(),StringSplitOptions.RemoveEmptyEntries))
            {
                var convertedNumber = int.Parse(number);

                if (convertedNumber<0)
                {
                    negativeNumbers = string.Join(" ", negativeNumbers, number);
                }

                sum += int.Parse(number);
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                throw new Exception("Negatives not Allowed." + negativeNumbers);
            }

            return sum;
        }
    }
}
